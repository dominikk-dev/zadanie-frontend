(function() {
    window.onload = function() {
        $('.owl-carousel').owlCarousel({
            loop:true,
            nav:true,
            items: 1,
            autoplay: true,
            autoplayTimeout: 3000,
            nav: false,            
        });
       
        // Fix owl in chrome
        window.dispatchEvent(new Event('resize'));

        var menuToggle = new Toggle('.component-nav__element');
        var tooltipToggle = new Toggle('.component-tooltip__element-link');
      };
})()
