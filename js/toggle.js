(function() {
    function Toggle(toggleItemsClassName) {
        this.toggleItemsClassName = toggleItemsClassName;
        this.init();
    }

    Toggle.prototype.init = function() {
        this.toggleItems = document.querySelectorAll(this.toggleItemsClassName);
        this.addEvents();
    };

    Toggle.prototype.addEvents = function() {
        this.toggleItems.forEach(function(item) {
            item.addEventListener('click', this.handleClick);
        }.bind(this));
    };

    Toggle.prototype.handleClick = function(evt) {
        evt.target.classList.toggle('toggle-active')
        var sibling = evt.target.nextElementSibling;
        if(sibling) {
            evt.preventDefault();
            sibling.classList.toggle('toggle-open');
        }
    }
    
    window.Toggle = Toggle;

})()